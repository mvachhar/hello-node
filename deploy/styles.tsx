import Adapt, { Style } from "@adpt/core";

import { Service } from "@adpt/cloud";
import { ServiceContainerSet } from "@adpt/cloud/docker";
import { makeClusterInfo, ServiceDeployment } from "@adpt/cloud/k8s";

export async function clusterInfo() {
    return makeClusterInfo({ registryUrl: process.env.KUBE_DOCKER_REPO || undefined });
}

/*
 * Laptop testing style - deploys to local Docker instance
 */
export const laptopStyle =
    <Style>
        {Service}
        {Adapt.rule(({ handle, ...remainingProps }) =>
            <ServiceContainerSet dockerHost={process.env.DOCKER_HOST} {...remainingProps} />)}
    </Style>;

/*
 * Kubernetes testing style
 */
export async function k8sTestStyle() {
    const info = await clusterInfo();
    return (
        <Style>
            {Service}
            {Adapt.rule((matchedProps) => {
                const { handle, ...remainingProps } = matchedProps;
                return <ServiceDeployment config={info} {...remainingProps} />;
            })}
        </Style>
    );
}
